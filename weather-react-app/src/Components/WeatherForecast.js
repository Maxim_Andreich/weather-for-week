import React from 'react';
import * as Api from 'typescript-fetch-api'

const api = new Api.DefaultApi()

class WeatherForecast extends React.Component {

constructor(props) {
super(props);
this.state = { weather: [] };

this.handleReload = this.handleReload.bind(this);
}


async handleReload(DailyWeather) {
const response = await api.weather({ date: '' });
this.setState({ weather: response });
}


render() {
return <div>
<table height="500px" border="1">
            <tr>
              <td>Monday</td>
              <td>Tuesday</td>
              <td>Wednesday</td>
              <td>Thursday</td>
              <td>Friday</td>
              <td>Saturday</td>
              <td>Sunday</td>
            </tr>
            <tr>
              <td>19.04</td>
              <td>20.04</td>
              <td>21.04</td>
              <td>22.04</td>
              <td>23.04</td>
              <td>24.04</td>
              <td>25.04</td>
            </tr>
            <tr>
              {this.state.weather.map((DailyWeather) => <td>{DailyWeather.temperature}</td>)}
              {this.state.weather.map((DailyWeather) => <td>{DailyWeather.temperature}</td>)}
              {this.state.weather.map((DailyWeather) => <td>{DailyWeather.temperature}</td>)}
              {this.state.weather.map((DailyWeather) => <td>{DailyWeather.temperature}</td>)}
              {this.state.weather.map((DailyWeather) => <td>{DailyWeather.temperature}</td>)}
              {this.state.weather.map((DailyWeather) => <td>{DailyWeather.temperature}</td>)}
              {this.state.weather.map((DailyWeather) => <td>{DailyWeather.temperature}</td>)}
            </tr>
            <tr>
              {this.state.weather.map((DailyWeather) => <td>{DailyWeather.location}</td>)}
              {this.state.weather.map((DailyWeather) => <td>{DailyWeather.location}</td>)}
              {this.state.weather.map((DailyWeather) => <td>{DailyWeather.location}</td>)}
              {this.state.weather.map((DailyWeather) => <td>{DailyWeather.location}</td>)}
              {this.state.weather.map((DailyWeather) => <td>{DailyWeather.location}</td>)}
              {this.state.weather.map((DailyWeather) => <td>{DailyWeather.location}</td>)}
              {this.state.weather.map((DailyWeather) => <td>{DailyWeather.location}</td>)}
            </tr>
          </table>
</div>
}
}

export default WeatherForecast;
