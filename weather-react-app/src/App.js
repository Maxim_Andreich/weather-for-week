import logo from './logo.svg';
import './App.css';
import React from 'react';
import WeatherForecast from './Components/WeatherForecast';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <section>
               <h1>Weather For Week: Minsk</h1>
               Welcome to our weather service. Here you can learn weather for this week.
          <WeatherForecast/>
        </section>
      </header>
    </div>
  );
  }
  

export default App;
